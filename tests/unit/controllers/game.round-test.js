import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | game.round', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:game.round');
    assert.ok(controller);
  });
});
